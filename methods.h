#ifndef METHODS_H
#define METHODS_H


class methods
{
public:
    methods();
    void BFS(int mapArr[3][3]);
    void RBFS(int mapArr[3][3]);
    void A_STAR(int mapArr[3][3]);
    bool isInOrder(int mapArr[3][3]);

    bool isUpPossible(int mapArr[3][3], int result[3][3]);
    bool isDownPossible(int mapArr[3][3], int result[3][3]);
    bool isRightPossible(int mapArr[3][3], int result[3][3]);
    bool isLeftPossible(int mapArr[3][3], int result[3][3]);
    int myH(int mapArr[3][3]);
};

#endif // METHODS_H
