#include <QCoreApplication>
#include <iostream>
#include <methods.h>

using namespace std;


enum hardness{
    EASY,
    MEDIUM,
    HARD
};

int mapArr[3][3];
void showMap();
void initMap(hardness mode);
hardness modeDefine();

int main()
{
    methods method;
    initMap(modeDefine());
    showMap();
    //method.BFS(mapArr);
    method.A_STAR(mapArr);
    showMap();
    return 0;

}
void initMap(hardness mode){
    if(mode == EASY){

        mapArr[0][0] = 1;
        mapArr[0][1] = 3;
        mapArr[0][2] = 4;

        mapArr[1][0] = 8;
        mapArr[1][1] = 6;
        mapArr[1][2] = 2;

        mapArr[2][0] = 7;
        mapArr[2][1] = 0;
        mapArr[2][2] = 5;

    }
    if(mode == MEDIUM){
        mapArr[0][0] = 2;
        mapArr[0][1] = 8;
        mapArr[0][2] = 1;

        mapArr[1][0] = 0;
        mapArr[1][1] = 4;
        mapArr[1][2] = 3;

        mapArr[2][0] = 7;
        mapArr[2][1] = 6;
        mapArr[2][2] = 5;
    }
    if(mode == HARD){
        mapArr[0][0] = 2;
        mapArr[0][1] = 8;
        mapArr[0][2] = 1;

        mapArr[1][0] = 4;
        mapArr[1][1] = 6;
        mapArr[1][2] = 3;

        mapArr[2][0] = 0;
        mapArr[2][1] = 7;
        mapArr[2][2] = 5;
    }
}
void showMap(){
    for(int i = 0 ; i < 3 ; i++){
        for(int j = 0 ; j < 3 ; j++){
            cout << "  ";
            if(mapArr[i][j] == 0){
                cout << " ";
            }
            else{
                cout << mapArr[i][j];
            }
            cout << "  ";
        }
        cout << endl << endl;
    }
}
hardness modeDefine(){
    hardness mode = HARD;
    cout << "game Difficulty : "<<endl<<"1 - easy"<<endl<<"2 - medium"<<endl<<"3 - hard"<<endl;
    int dif;
    cin >> dif;
    if(dif == 1){
        mode = EASY;
    }
    if(dif == 2){
        mode = MEDIUM;
    }
    return mode;
}
