#include "methods.h"
#include "node.h"
#include <vector>
#include <queue>
#include <iostream>
using namespace std;
methods::methods()
{

}

void methods::BFS(int mapArr[3][3]){

    //vector<node> Q;

    queue <node> Q;

    node curNode;

    //do copy map starts
    for(int i = 0 ; i < 3 ; i++){
        for(int j = 0 ; j < 3 ; j++){
            curNode.m[i][j] = mapArr[i][j];
        }
    }
    //do copy map ends

    Q.push(curNode);
    curNode = Q.front();
    Q.pop();
    int count = 0;
    while (!isInOrder(curNode.m)) {

        node tmpNode;

        if(isUpPossible(curNode.m , tmpNode.m)){
            tmpNode.path.clear();
            for(int i = 0 ; i < curNode.path.size() ; i++){
                tmpNode.path.push_back(curNode.path.at(i));
            }
            tmpNode.path.push_back("UP");
            Q.push(tmpNode);
            count++;
        }
        if(isDownPossible(curNode.m , tmpNode.m)){
            tmpNode.path.clear();
            for(int i = 0 ; i < curNode.path.size() ; i++){
                tmpNode.path.push_back(curNode.path.at(i));
            }
            tmpNode.path.push_back("DOWN");
            Q.push(tmpNode);
            count++;
        }
        if(isRightPossible(curNode.m , tmpNode.m)){
            tmpNode.path.clear();
            for(int i = 0 ; i < curNode.path.size() ; i++){
                tmpNode.path.push_back(curNode.path.at(i));
            }
            tmpNode.path.push_back("RIGHT");
            Q.push(tmpNode);
            count++;
        }
        if(isLeftPossible(curNode.m , tmpNode.m)){
            tmpNode.path.clear();
            for(int i = 0 ; i < curNode.path.size() ; i++){
                tmpNode.path.push_back(curNode.path.at(i));
            }
            tmpNode.path.push_back("LEFT");
            Q.push(tmpNode);
            count++;
        }


        curNode = Q.front();
        Q.pop();

    }

    //show map

    for(int i = 0 ; i < 3 ; i++){
        for(int j = 0 ; j < 3 ; j++){
            mapArr[i][j] = curNode.m[i][j];
        }

    }


    for(int i = 0 ; i < curNode.path.size() ; i++){
        cout << curNode.path.at(i)<<" - ";
    }

    cout << "node checked : "<< count << endl;

    //show map

}

void methods::RBFS(int mapArr[3][3]){

}

void methods::A_STAR(int mapArr[3][3]){
    vector<node> Q;

    //queue <node> Q;

    node curNode;

    //do copy map starts
    for(int i = 0 ; i < 3 ; i++){
        for(int j = 0 ; j < 3 ; j++){
            curNode.m[i][j] = mapArr[i][j];
        }
    }
    //do copy map ends

    Q.push_back(curNode);
    curNode = Q.front();
    Q.pop_back();
    int count = 0;
    while (!isInOrder(curNode.m)) {

        node tmpNode;

        if(isUpPossible(curNode.m , tmpNode.m)){
            tmpNode.path.clear();
            for(int i = 0 ; i < curNode.path.size() ; i++){
                tmpNode.path.push_back(curNode.path.at(i));
            }
            tmpNode.path.push_back("UP");
            Q.push_back(tmpNode);
            count++;
        }
        if(isDownPossible(curNode.m , tmpNode.m)){
            tmpNode.path.clear();
            for(int i = 0 ; i < curNode.path.size() ; i++){
                tmpNode.path.push_back(curNode.path.at(i));
            }
            tmpNode.path.push_back("DOWN");
            Q.push_back(tmpNode);
            count++;
        }
        if(isRightPossible(curNode.m , tmpNode.m)){
            tmpNode.path.clear();
            for(int i = 0 ; i < curNode.path.size() ; i++){
                tmpNode.path.push_back(curNode.path.at(i));
            }
            tmpNode.path.push_back("RIGHT");
            Q.push_back(tmpNode);
            count++;
        }
        if(isLeftPossible(curNode.m , tmpNode.m)){
            tmpNode.path.clear();
            for(int i = 0 ; i < curNode.path.size() ; i++){
                tmpNode.path.push_back(curNode.path.at(i));
            }
            tmpNode.path.push_back("LEFT");
            Q.push_back(tmpNode);
            count++;
        }


        //curNode = Q.front();
        //Q.pop();

        int myBestH = 999;
        int myBestHIndex = -1;



        for(int i = 0 ; i < Q.size() ; i++){
            if(myH(Q.at(i).m) + Q.at(i).path.size() < myBestH){
                myBestH = myH(Q.at(i).m) + Q.at(i).path.size();
                myBestHIndex = i;
            }

        }

        //cout << myBestHIndex << endl;


        curNode = Q.at(myBestHIndex);
        Q.erase(Q.begin() + myBestHIndex);




    }

    //show map

    for(int i = 0 ; i < 3 ; i++){
        for(int j = 0 ; j < 3 ; j++){
            mapArr[i][j] = curNode.m[i][j];
        }

    }


    for(int i = 0 ; i < curNode.path.size() ; i++){
        cout << curNode.path.at(i)<<" - ";
    }

    cout << "node checked : "<< count << endl;

    //show map

}

bool methods::isInOrder(int mapArr[3][3]){
    bool isOk = true;
    if(mapArr[0][0] != 1){
        isOk = false;
    }
    if(mapArr[0][1] != 2){
        isOk = false;
    }
    if(mapArr[0][2] != 3){
        isOk = false;
    }
    if(mapArr[1][0] != 8){
        isOk = false;
    }
    if(mapArr[1][2] != 4){
        isOk = false;
    }
    if(mapArr[2][2] != 5){
        isOk = false;
    }
    if(mapArr[2][1] != 6){
        isOk = false;
    }
    if(mapArr[2][0] != 7){
        isOk = false;
    }
    return isOk;
}


//check possible sides
bool methods::isUpPossible(int mapArr[3][3] , int result[3][3]){
    int x , y;
    for(int i = 0 ; i < 3 ; i++){
        for(int j =0 ; j < 3 ; j++){
            result[i][j] = mapArr[i][j];
            if(mapArr[i][j] == 0){
                y = i;
                x = j;
            }
        }
    }

    if(y == 0){
        return false;
    }



    result[y][x] = result[y-1][x];
    result[y-1][x] = 0;


    return true;

}
bool methods::isDownPossible(int mapArr[3][3] , int result[3][3]){
    int x , y;
    for(int i = 0 ; i < 3 ; i++){
        for(int j =0 ; j < 3 ; j++){
            result[i][j] = mapArr[i][j];
            if(mapArr[i][j] == 0){
                y = i;
                x = j;
            }
        }
    }
    if( y == 2){
        return false;
    }

    result[y][x] = result[y+1][x];
    result[y+1][x] = 0;

    return true;
}
bool methods::isRightPossible(int mapArr[3][3] , int result[3][3]){
    int x , y;
    for(int i = 0 ; i < 3 ; i++){
        for(int j =0 ; j < 3 ; j++){
            result[i][j] = mapArr[i][j];
            if(mapArr[i][j] == 0){
                y = i;
                x = j;
            }
        }
    }
    if(x == 2){
        return false;
    }

    result[y][x] = result[y][x+1];
    result[y][x+1] = 0;

    return true;

}
bool methods::isLeftPossible(int mapArr[3][3] , int result[3][3]){
    int x , y;
    for(int i = 0 ; i < 3 ; i++){
        for(int j =0 ; j < 3 ; j++){
            result[i][j] = mapArr[i][j];
            if(mapArr[i][j] == 0){
                y = i;
                x = j;
            }
        }
    }
    if(x == 0){
        return false;
    }


    result[y][x] = result[y][x-1];
    result[y][x-1] = 0;


    return true;

}

int methods::myH(int mapArr[3][3]){
    int c = 8;
    if(mapArr[0][0] == 1){
        c--;
    }
    if(mapArr[0][1] == 2){
        c--;
    }
    if(mapArr[0][2] == 3){
        c--;
    }
    if(mapArr[1][0] == 8){
        c--;
    }
    if(mapArr[1][2] == 4){
        c--;
    }
    if(mapArr[2][2] == 5){
        c--;
    }
    if(mapArr[2][1] == 6){
        c--;
    }
    if(mapArr[2][0] == 7){
        c--;
    }
    return c;
}

//check possible sides






