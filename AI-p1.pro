QT += core
QT -= gui

CONFIG += c++11

TARGET = AI-p1
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    methods.cpp \
    node.cpp

HEADERS += \
    methods.h \
    node.h
